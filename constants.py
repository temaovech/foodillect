from config import Config

REDIS_BROKER_URL = f"redis://{Config.REDIS_USER}:{Config.REDIS_PASSWORD}@{Config.REDIS_HOST}:{Config.REDIS_PORT}"
