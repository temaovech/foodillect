import os


def delete_image(image):
    """
    Remove image from folder
    :param image: image to be removed
    """
    image.close()
    os.remove(image.name)
