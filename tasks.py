from celery import Celery
from celery.schedules import crontab
from telebot.apihelper import ApiException

from bot import bot
from config import Config
from constants import REDIS_BROKER_URL
from image_handler import set_image_pack, get_image
from utils import delete_image

app = Celery('tasks', broker=REDIS_BROKER_URL)


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    """Seyup Celery crone tasks"""
    sender.add_periodic_task(crontab(minute='*/10'), send_image.s(), name='Send image every 10 minutes')
    sender.add_periodic_task(crontab(minute='*/63'), update_packs.s(), name='Update main pack every 63 minutes')
    sender.add_periodic_task(crontab(minute='*/113'), update_packs.s(True),
                             name='Update reserved pack every 113 minutes')


@app.task(bind=True, autoretry_for=(ApiException,))
def send_image(self):
    """Send image to channel"""
    image = get_image()
    try:
        bot.send_photo(Config.FOODILLECT_CHAT_ID, image)
        delete_image(image)
    except Exception as exc:
        print("error_image: ", image)
        delete_image(image)
        raise self.retry(exc=exc, countdown=1)


@app.task
def update_packs(reversed=False):
    """
    Update image folders
    :param reversed: flag to detect if we need to update reserved folder
    """
    set_image_pack(reversed)
