FROM python:3
ARG BOT_TOKEN
ENV BOT_TOKEN=$BOT_TOKEN
ARG CHAT_ID
ENV CHAT_ID=$CHAT_ID
ARG API_KEY
ENV API_KEY=$API_KEY
ARG REDIS_USER
ENV REDIS_USER=$REDIS_USER
ARG REDIS_PASSWORD
ENV REDIS_PASSWORD=$REDIS_PASSWORD
ARG REDIS_HOST
ENV REDIS_HOST=$REDIS_HOST
ARG REDIS_PORT
ENV REDIS_PORT=$REDIS_PORT
RUN mkdir -p /code/images/reserved
WORKDIR /code
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
RUN python -c "from image_handler import set_image_pack; set_image_pack(); set_image_pack(True)"
CMD celery -A tasks worker --loglevel=info -B