import os
import random

import requests
from pyunsplash import PyUnsplash

from config import Config


def set_image_pack(reserved=False):
    """
    Initialize image pack
    :param reserved: flag that sets option to full 'reserved' folder via images or not
    """
    pu = PyUnsplash(api_key=Config.UNSPLASH_KEY)
    image = pu.photos(type_='random', count=25, featured=True, query="food")
    for photo in image.entries:
        url = photo.link_download
        r = requests.get(url)
        if r.status_code == 200:
            try:
                filename = f'images/reserved/{url.split("/")[4]}.jpg' if reserved \
                    else f'images//{url.split("/")[4]}.jpg'
                with open(filename, 'wb') as f:
                    f.write(r.content)
            except Exception as e:
                # TODO: add logging for errors
                print('ERROR OCCURRED: ', e)


def get_image():
    """Get random image from folder"""
    try:
        image = None
        dir = "images/"
        filename = random.choice(os.listdir(dir))
        if not filename:
            dir = "images/reserved"
            filename = random.choice(os.listdir(dir))

        if filename:
            image = open(f"{dir}{filename}", 'rb')
        return image or filename

    except Exception as e:
        # TODO: add logging for errors
        print('ERROR OCCURRED: ', e)


# Run script manually:
if __name__ == '__main__':
    set_image_pack()
    set_image_pack(reserved=True)
    a = get_image()
