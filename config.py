import os


class Config:
    """Application config class"""
    # Redis Config
    REDIS_USER = os.environ.get("REDIS_USER")
    REDIS_PASSWORD = os.environ.get("REDIS_PASSWORD")
    REDIS_HOST = os.environ.get("REDIS_HOST")
    REDIS_PORT = os.environ.get("REDIS_PORT")

    # Unsplash constants
    UNSPLASH_KEY = os.environ.get('API_KEY')

    # Telegram constants
    BOT_TOKEN = os.environ.get('BOT_TOKEN')
    FOODILLECT_CHAT_ID = os.environ.get('CHAT_ID')
